import './App.css';
import React, { useState , useMemo} from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { ProvideAuth } from "./context/AuthContext";
import Login from "./components/Login";
import PrivateRoute from "./components/PrivateRoute";
import {UserNameContext} from "./context/UserNameContext"
function App() {
  const [userNameGlobal, setUserNameGlobal] = useState("context")
  const providerValue = useMemo(() => ({userNameGlobal, setUserNameGlobal}), [userNameGlobal, setUserNameGlobal])
  console.log(userNameGlobal,"GLOBAL NAME   CONTEXT")
  return (
    <BrowserRouter>
{/* Backwards doesnt work  */}
<ProvideAuth>
  <UserNameContext.Provider value={providerValue}>
      {/* <BrowserRouter> */}
        {/* <AuthProvider> */}
        <Switch>
          <Route exact path="/" component={Login}>
          </Route>

          <Route   exact path="/home" component={PrivateRoute}>
          </Route>
        </Switch>
        {/* </AuthProvider> */}
      {/* </BrowserRouter> */}
      </UserNameContext.Provider>
    </ProvideAuth>
    </BrowserRouter>
  );
}

export default App;
