import React, { useContext, createContext, useState } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useHistory,
  useLocation
} from "react-router-dom";

const AuthContext = React.createContext();



export function useAuth() {
  return useContext(AuthContext);
}

export function ProvideAuth({ children }) {
  const auth = useProvideAuth();
  return (
    <AuthContext.Provider value={auth}>
      {children}
    </AuthContext.Provider>
  );
}

const fakeAuth = {
  isAuthenticated: false,
  signin(cb) {
    fakeAuth.isAuthenticated = true;
    setTimeout(cb, 100); // fake async
  },
  signout(cb) {
    fakeAuth.isAuthenticated = false;
    setTimeout(cb, 100);
  }
};

function useProvideAuth() {
  const [user, setUser] = useState(null);

  const signin = cb => {
    return fakeAuth.signin(() => {
      setUser("user");
      cb();
    });
  };

  const signout = cb => {
    return fakeAuth.signout(() => {
      setUser(null);
      cb();
    });
  };

  return {
    user,
    signin,
    signout
  };
}


function AuthButton() {
  let history = useHistory();
  let auth = useAuth();

  return auth.user ? (
    <p>
      Welcome!{" "}
      <button
        onClick={() => {
          auth.signout(() => history.push("/"));
        }}
      >
        Sign out
      </button>
    </p>
  ) : (
    <p>You are not logged in.</p>
  );
}




// function LoginPage() {
//   let history = useHistory();
//   let location = useLocation();
//   let auth = useAuth();

//   let { from } = location.state || { from: { pathname: "/" } };
//   let login = () => {
//     auth.signin(() => {
//       history.replace(from);
//     });
//   };

//   return (
//     <div>
//       <p>You must log in to view the page at {from.pathname}</p>
//       <button onClick={login}>Log in</button>
//     </div>
//   );
// }


// export function AuthProvider({ children }) {
//   const [currentUser, setCurrentUser] = useState("");
//   const [loading, setLoading] = useState(true);

//   // function signup(email, password) {
//   //   return auth.createUserWithEmailAndPassword(email, password);
//   // }

//   // function login(email, password) {
//   //   return auth.signInWithEmailAndPassword(email, password);
//   // }

//   // function logout(e) {
//   //   return auth.signOut();
//   // }

//   // function resetPassword(email) {
//   //   return auth.sendPasswordResetEmail(email);
//   // }

//   // async function updateEmail(email) {
//   //   return await currentUser.updateEmail(email);
//   // }

//   // function updatePassword(password) {
//   //   return currentUser.updatePassword(password);
//   // }

//   // useEffect(() => {
//   //   const unsubscribe = auth.onAuthStateChanged((user) => {
//   //     setCurrentUser(user);
//   //     setLoading(false);
//   //   });

//   //   return unsubscribe;
//   // }, []);

//   const sampleAppContext = {
//     currentUser,
//     // login,
//     // signup,
//     // logout,
//     // resetPassword,
//     // updateEmail,
//     // updatePassword,
//   };

//   return (
//     <AuthContext.Provider value={sampleAppContext}>
//       {!loading && children}
//     </AuthContext.Provider>
//   );
// }
