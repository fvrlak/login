import React, { Component, useState, useEffect, useContext} from "react";
import {UserNameContext} from "../../context/UserNameContext"
export default function Home() {

  const context = useContext(UserNameContext)
  // const welcome = JSON.stringify( context)
  // console.log(context) 
  return (
    <div>{context && <div>{context.userNameGlobal}</div>}</div>
  )
 
}

