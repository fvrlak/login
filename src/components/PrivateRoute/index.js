import React from "react"
import { Route, Redirect, RouteProps} from "react-router-dom"
import { useAuth } from "../../context/AuthContext"
import Home from "../Home"
const PrivateRoute = ({ component: Component, ...rest }) => {
    const { currentUser } = useAuth()
    const v = true
      return (
        <Route
          {...rest}
          render={props => {
            return v === true ? <Home></Home> : <Redirect to="/" />
          }}
        ></Route>
      )
  }

export default PrivateRoute
